FROM openjdk:8
ENV jarPath target/pma-core.jar
ADD $jarPath pma-core.jar
EXPOSE 80
CMD exec java -jar pma-core.jar