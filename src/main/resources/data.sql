insert into document (creation_date, is_active, name, storage_url, update_date) values ('2019-04-03 00:00:00', 1, 'doc1', 'https://res.cloudinary.com/dmy5pjkae/image/upload/v1555358125/83959218-muscular-man-flexing-biceps-avatar-fitness-icon-image-vector-illustration-design_cpeokg.jpg', '2019-04-05 00:00:00');

insert into document (creation_date, is_active, name, storage_url, update_date) values ('2019-04-03 00:00:00', 1, 'doc2', 'https://res.cloudinary.com/dmy5pjkae/image/upload/v1555358125/83959218-muscular-man-flexing-biceps-avatar-fitness-icon-image-vector-illustration-design_cpeokg.jpg', '2019-04-05 00:00:00');

insert into document (creation_date, is_active, name, storage_url, update_date) values ('2019-04-03 00:00:00', 1, 'doc2', 'https://res.cloudinary.com/dmy5pjkae/image/upload/v1555358125/83959218-muscular-man-flexing-biceps-avatar-fitness-icon-image-vector-illustration-design_cpeokg.jpg', '2019-04-05 00:00:00');

insert into gym (latitude, longitude, name) values (45.248654, 19.844313, 'Flex');
insert into gym (latitude, longitude, name) values (45.250624, 19.838675, 'Miss Fit');
insert into gym (latitude, longitude, name) values (45.260209, 19.809622, 'Synergy');

insert into user (dtype, email, is_active, last_name, name, password, registration_date, username, profile_picture_id) 
	values ("trainer","mm@example.com", 1,"Markovic","Milica", "$2a$10$kcbiRDMhWUZ5a.gGrSh8I.Mm21lgxzdiSBiWfzNgcXbTK//858vey", '2019-04-03 00:00:00', "milicica", 1);
    
insert into user (dtype, email, is_active, last_name, name, password, registration_date, username, profile_picture_id) 
	values ("trainee","mmm@example.com", 1,"Markovic","Mica", "$2a$10$kcbiRDMhWUZ5a.gGrSh8I.Mm21lgxzdiSBiWfzNgcXbTK//858vey", '2019-04-04 00:00:00', "milicica94", 2);
    
insert into user (dtype, email, is_active, last_name, name, password, registration_date, username, profile_picture_id) 
	values ("trainer","mile@example.com", 1,"Trninic","Milorad", "$2a$10$kcbiRDMhWUZ5a.gGrSh8I.Mm21lgxzdiSBiWfzNgcXbTK//858vey", '2019-04-03 00:00:00', "milorad", 3);
    
insert into trainer (id, bio, gym_id) values(1,"bio", 2);
insert into trainee (id) values (2);
insert into trainer (id, bio, gym_id) values(3,"bio", 1);
insert into message (body, is_open, sent_date, receiver_id, sender_id) values ("Cao",1,'2019-04-05 00:00:00',1,2);

insert into time_slot (day,hour,month,year, trainer_id,active) values(11, 8,4,2019,1,true);

insert into timeslot_trainee (time_slot_id, trainee_id,allowed) values (1,2,true);

select * from document;

select * from user;

update document set trainer_id = 1 where id = 1; 
update document set trainer_id = 1 where id = 2; 
update document set trainer_id = 3 where id = 2; 

insert into comment (body, creation_date, is_active, update_date, create_by_id, time_slot_id) 
	values ("comm", '2019-04-05 00:00:00',1,'2019-04-06 00:00:00',1,1);    
    
insert into rating (rating_number, time_slot_id, trainee_id) values (4.23, 1,2);