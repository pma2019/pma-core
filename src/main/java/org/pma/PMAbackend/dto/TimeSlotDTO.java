package org.pma.PMAbackend.dto;

import org.hibernate.validator.constraints.Range;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TimeSlotDTO {
	private Integer id;
	@Range(min = 2019, max = 2030)
	private Integer year;
	@Range(min = 1, max = 12)
	private Integer month;
	@Range(min = 1, max = 31)
	private Integer day;
	@Range(min = 0, max = 23)
	private Integer hour;
	private boolean active;

	private Integer trainerId;

	private String trainerName;

	private String trainerBio;

	private String trainerProfilePictureStorageUrl;

	private String trainerGymName;

}
