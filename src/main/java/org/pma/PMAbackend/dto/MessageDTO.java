package org.pma.PMAbackend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter @Setter @NoArgsConstructor
public class MessageDTO {
    private Integer id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private Date sentDate;
    private Boolean isOpen;
    private String body;
    private String senderEmail;
    private String receiverEmail;
    private String senderUsername;
    private String receiverUsername;
    private String receiverProfilePictureStorageUrl;
    private String senderProfilePictureStorageUrl;
}
