package org.pma.PMAbackend.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter @Setter
public class TrainerBasicDTO {
    private String username;
    private String email;
    private String name;
    private String lastName;

}
