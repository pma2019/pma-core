package org.pma.PMAbackend.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor @Getter @Setter
public class RatingDTO {
    private Integer id;
    private Float ratingNumber;
    private String traineeEmail;
    private Integer timeSlotId;

}
