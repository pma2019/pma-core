package org.pma.PMAbackend.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.Date;

@Getter @Setter @NoArgsConstructor
public class UserDTO {

    private Integer id;
    private String username;
    private String password;
    private String email;
    private String name;
    private String lastName;
    private Date registrationDate;
    private Boolean isActive;

    private String profilePictureStorageUrl;

    private String DTYPE;
}
