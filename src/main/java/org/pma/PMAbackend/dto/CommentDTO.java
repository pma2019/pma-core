package org.pma.PMAbackend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class CommentDTO {
    private Integer id;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private Date creationDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private Date updateDate;
    private Boolean isActive;
    private String body;
    private Boolean byTrainer;

    private Integer timeSlotId;

    private String createByEmail;

    private String createByUsername;

    private String createByProfilePictureStorageUrl;

    private CommentDTO reply;
}
