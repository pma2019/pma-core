package org.pma.PMAbackend.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class DocumentDTO {
    private Integer id;
    private String name;
    private String storageUrl;
    private Date creationDate;
    private Date updateDate;
    private Boolean isActive;
}
