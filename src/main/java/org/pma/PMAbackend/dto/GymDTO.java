package org.pma.PMAbackend.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
public class GymDTO {
	private Integer id;

    private String name;

    private Double longitude;

    private Double latitude;

    private ArrayList<TrainerBasicDTO> trainers;
}
