package org.pma.PMAbackend.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class TimeSlotTraineeDTO {
    private String traineeEmail;
    private String traineeUsername;
    private String traineeProfilePictureStorageUrl;
    private Integer timeSlotId;
    private Integer timeSlotYear;
    private Integer timeSlotMonth;
    private Integer timeSlotDay;
    private Integer timeSlotHour;
    private String timeSlotTrainerGymName;
    private String timeSlotTrainerName;
    private String timeSlotTrainerBio;
    private String timeSlotTrainerProfilePictureStorageUrl;
    private Boolean allowed;
}
