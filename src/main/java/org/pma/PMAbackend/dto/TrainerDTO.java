package org.pma.PMAbackend.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TrainerDTO extends UserDTO {
    private String gymName;
    private Double gymLongitude;
    private Double gymLatitude;
    private String bio;
    private List<DocumentDTO> pictures;
    private Float overallRatings;
}
