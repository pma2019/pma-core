package org.pma.PMAbackend.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter @Setter
public class AddTimeslotTrainee {
    private String email;
    private Integer timeSlotId;

}
