package org.pma.PMAbackend.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AllowTimeSlot {
    private String traineeEmail;
    private Integer timeSlotId;
}
