package org.pma.PMAbackend.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class UserChangePassword {

    private String email;
    private String oldPassword;
    private String newPassword;
}
