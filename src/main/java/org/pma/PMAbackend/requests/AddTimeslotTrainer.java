package org.pma.PMAbackend.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter @Setter
public class AddTimeslotTrainer {
	private String trainerEmail;
	private Integer year;
	private Integer month;
	private Integer day;
	private Integer hour;
}
