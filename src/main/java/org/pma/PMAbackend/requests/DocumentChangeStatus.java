package org.pma.PMAbackend.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DocumentChangeStatus {
    private Integer id;
    private Boolean status;
}
