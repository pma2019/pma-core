package org.pma.PMAbackend.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class CommentChangeStatus {
    private Integer id;
    private Boolean status;
}
