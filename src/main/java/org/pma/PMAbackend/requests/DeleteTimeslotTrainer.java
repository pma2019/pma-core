package org.pma.PMAbackend.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class DeleteTimeslotTrainer {

	private String trainerEmail;
	private Integer timeSlotId;
}
