package org.pma.PMAbackend.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class AddComment {

    private Integer timeSlotId;
    private String body;
    private String createByEmail;
    private Boolean byTrainer;
    private Integer parentId;
}
