package org.pma.PMAbackend.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class UserChangeStatus {
    private String email;
    private Boolean status;
}
