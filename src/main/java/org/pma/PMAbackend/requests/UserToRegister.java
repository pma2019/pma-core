package org.pma.PMAbackend.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class UserToRegister {

	    private String username;
	    private String password;
	    private String email;
	    private String name;
	    private String lastName;
	    private String dtype;
	    private Integer gymId;
	    private String bio;

}
