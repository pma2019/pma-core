package org.pma.PMAbackend.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter @Setter
public class AddRating {
    private Float ratingNumber;
    private String traineeEmail;
    private Integer timeSlotId;
}
