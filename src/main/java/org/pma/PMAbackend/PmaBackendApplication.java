package org.pma.PMAbackend;

import com.cloudinary.Cloudinary;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class PmaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(PmaBackendApplication.class, args);
	}

	@Bean(value = "modelMapper")
	public ModelMapper getModelMapper(){
		return new ModelMapper();
	}

	@Bean(value = "cloudinary")
	public Cloudinary cloudinary() {
		Map<String, String> config = new HashMap<String, String>();
		config.put("cloud_name", "dmy5pjkae");
		config.put("api_key", "829992516448526");
		config.put("api_secret", "1iwHzHgnhKgg2wvrM8WAMTMbgDs");
		return new Cloudinary(config);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
						.allowedOrigins("*")
						.allowedMethods("*")
						.allowedHeaders("*")
						.exposedHeaders("Content-Type", "Date", "Total-Count", "Authorization")
						.maxAge(3600);
			}
		};
	}
}
