package org.pma.PMAbackend.controller;

import org.modelmapper.ModelMapper;
import org.pma.PMAbackend.dto.GymDTO;
import org.pma.PMAbackend.model.Gym;
import org.pma.PMAbackend.repo.GymRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/gyms")
public class GymController {
    @Autowired
    GymRepo gymRepo;

    @Autowired
    ModelMapper mapper;


    @GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAll() {
    	List<Gym> p = gymRepo.findAll();
        return ResponseEntity.ok(p.stream().map(d -> mapper.map(d, GymDTO.class)));
    }
}
