package org.pma.PMAbackend.controller;


import org.modelmapper.ModelMapper;
import org.pma.PMAbackend.dto.RatingDTO;
import org.pma.PMAbackend.model.Rating;
import org.pma.PMAbackend.model.TimeSlot;
import org.pma.PMAbackend.model.Trainee;
import org.pma.PMAbackend.model.TraineeTimeSlot;
import org.pma.PMAbackend.repo.RatingRepo;
import org.pma.PMAbackend.repo.TimeSlotRepo;
import org.pma.PMAbackend.repo.TraineeRepo;
import org.pma.PMAbackend.repo.TraineeTimeSlotRepo;
import org.pma.PMAbackend.requests.AddRating;
import org.pma.PMAbackend.responses.OverAllRating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

@RestController
@RequestMapping(value="ratings")
public class RatingController {
    @Autowired
    RatingRepo ratingRepo;

    @Autowired
    TimeSlotRepo timeSlotRepo;

    @Autowired
    TraineeRepo traineeRepo;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    TraineeTimeSlotRepo traineeTimeSlotRepo;

    @GetMapping(value = "findByTimeSlot", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByTimeSlot(@RequestParam("timeSlotId") Integer id) {
        return ResponseEntity.ok(ratingRepo.findByTimeSlotId(id));
    }

    @GetMapping(value = "findByTrainer", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByUser(@RequestParam("email") String email) {
        return ResponseEntity.ok(ratingRepo.findByTimeSlotTrainerEmail(email));
    }

    @GetMapping(value = "findByTraineeTimeSlot", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByTraineeTimeSlot(@RequestParam("traineeEmail")String email, @RequestParam("timeSlotId") Integer id) {
        Optional<Rating> ratingOptional = ratingRepo.findByTimeSlotIdAndTraineeEmail(id, email);

        if(!ratingOptional.isPresent()){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(modelMapper.map(ratingOptional.get(), RatingDTO.class));
    }

    @GetMapping(value = "findByTrainerTimeSlot", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findByTrainerTimeSlot(@RequestParam("trainerEmail")String email, @RequestParam("timeSlotId") Integer id) {
        Optional<TimeSlot> timeSlot = timeSlotRepo.findByIdAndActive(id, true);
        if(!timeSlot.isPresent()){
            return ResponseEntity.notFound().build();
        }
        OverAllRating overAllRating = new OverAllRating();
        overAllRating.setTimeSlotId(id);
        overAllRating.setTrainerEmail(email);
        overAllRating.setOverAllRating(timeSlot.get().getOverAllRatings());


        return ResponseEntity.ok(overAllRating);
    }


    @PostMapping(value = "addRating", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> traineeAddRating(@RequestBody AddRating request) {
        Rating rating = new Rating();
        if(request.getRatingNumber() > 5.0F || request.getRatingNumber() < 0.0F) {
            return ResponseEntity.badRequest().body("Rating should be between 0 and 5");
        }
        rating.setRatingNumber(request.getRatingNumber());
        Optional<Rating> alreadyRated = ratingRepo.findByTimeSlotIdAndTraineeEmail(request.getTimeSlotId(), request.getTraineeEmail());
        if(alreadyRated.isPresent()) {
            return ResponseEntity.badRequest().body("Already rated this appointment");
        }
        Optional<TimeSlot> timeSlot = timeSlotRepo.findByIdAndTraineesTraineeEmailAndTraineesAllowed(request.getTimeSlotId(),
                request.getTraineeEmail(),
                true);
        if(!timeSlot.isPresent()) {
            return ResponseEntity.badRequest().body("Time slot not present");
        }
        LocalDate date = LocalDate.now();
        LocalDate timeSlotDate = LocalDate.of(timeSlot.get().getYear(), timeSlot.get().getMonth(), timeSlot.get().getDay());

        if(!date.isAfter(timeSlotDate) && LocalTime.now().getHour() < timeSlot.get().getHour()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        Optional<Trainee> trainee = traineeRepo.findByEmail(request.getTraineeEmail());
        if(!trainee.isPresent()) {
            return ResponseEntity.badRequest().body("Trainee not present");
        }
        rating.setTimeSlot(timeSlot.get());
        rating.setTrainee(trainee.get());
        ratingRepo.save(rating);

        return ResponseEntity.ok(modelMapper.map(rating, RatingDTO.class));
    }


}
