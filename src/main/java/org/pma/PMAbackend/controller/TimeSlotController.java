package org.pma.PMAbackend.controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.pma.PMAbackend.dto.TimeSlotDTO;
import org.pma.PMAbackend.dto.TimeSlotTraineeDTO;
import org.pma.PMAbackend.model.TimeSlot;
import org.pma.PMAbackend.model.Trainee;
import org.pma.PMAbackend.model.TraineeTimeSlot;
import org.pma.PMAbackend.model.Trainer;
import org.pma.PMAbackend.repo.TimeSlotRepo;
import org.pma.PMAbackend.repo.TraineeRepo;
import org.pma.PMAbackend.repo.TraineeTimeSlotRepo;
import org.pma.PMAbackend.repo.TrainerRepo;
import org.pma.PMAbackend.repo.UserRepo;
import org.pma.PMAbackend.requests.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/appointments")
public class TimeSlotController {

	@Autowired
	TimeSlotRepo timeSlotRepo;

	@Autowired
	ModelMapper mapper;

	@Autowired
	UserRepo userRepo;

	@Autowired
	TrainerRepo trainerRepo;

	@Autowired
	TraineeRepo traineeRepo;

	@Autowired
	TraineeTimeSlotRepo traineeTimeSlotRepo;

	@GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getAll(Pageable pageable) {
		return ResponseEntity.ok(timeSlotRepo.findAll(pageable).map(d -> mapper.map(d, TimeSlotDTO.class)));
	}

	@GetMapping(value = "byTrainer", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getAllByTrainer(@RequestParam(value = "email") String email) {
		return ResponseEntity.ok(timeSlotRepo.findAllByTrainerEmailAndActive(email, true).stream()
				.map(d -> mapper.map(d, TimeSlotDTO.class)));
	}

	@GetMapping(value = "byTrainee", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getAllByTrainee(@RequestParam(value = "email") String email) {
		return ResponseEntity.ok(traineeTimeSlotRepo.findAllByTraineeEmail(email).stream()
				.map(d -> mapper.map(d, TimeSlotTraineeDTO.class)));
	}

	@SuppressWarnings("unused")
	@PostMapping(value = "addtimeslottrainer", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = "application/json")
	public ResponseEntity<?> addTimeslotTrainer(@RequestBody AddTimeslotTrainer request) {
		Optional<Trainer> user = trainerRepo.findByEmail(request.getTrainerEmail());
		if (!user.isPresent()) {
			return ResponseEntity.notFound().build();
		} else {
			if (user.get().getEmail().equals(request.getTrainerEmail())) {
				Trainer t = user.get();
				LocalDate date1 = LocalDate.now();
				LocalDate requestDate = LocalDate.of(request.getYear(), request.getMonth(), request.getDay());
				if (date1.isBefore(requestDate) || (date1.getYear() <= request.getYear()
						&& date1.getMonthValue() <= request.getMonth() && date1.getDayOfMonth() <= request.getDay()
						&& request.getHour() > LocalTime.now().getHour())) {
					TimeSlot converted = new TimeSlot();
					converted.setDay(request.getDay());
					converted.setMonth(request.getMonth());
					converted.setHour(request.getHour());
					converted.setYear(request.getYear());
					converted.setActive(true);
					converted.setTrainer(t);
					List<TimeSlot> x=timeSlotRepo.findAll();
					//TODO Proveriti da li vec postoji time slot u zadatom vremenu.
					for(int i=0;i<x.size();i++) {
						if(x.get(i).getYear().equals(converted.getYear()) &&
								x.get(i).getMonth().equals(converted.getMonth()) &&
								x.get(i).getDay().equals(converted.getDay()) &&
								x.get(i).getHour().equals(converted.getHour()) &&
								x.get(i).getTrainer().getEmail().equals(converted.getTrainer().getEmail())) {
							return ResponseEntity.badRequest().build();	
						}
						else {
							continue;
						}
					}
					timeSlotRepo.save(converted);
					user.get().getTimeSlots().add(converted);
					trainerRepo.save(user.get());
					return ResponseEntity.ok(mapper.map(converted, TimeSlotDTO.class));

				} else {
					return ResponseEntity.badRequest().build();
				}
			} else {
				return ResponseEntity.notFound().build();
			}
		}
	}

	@PostMapping(value = "addtimeslottrainee", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> addTimeslotTrainee(@RequestBody AddTimeslotTrainee request) {
		Optional<Trainee> user = traineeRepo.findByEmail(request.getEmail());
		if (!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Trainee trainee = user.get();
		Optional<TimeSlot> timeSlot = timeSlotRepo.findByIdAndActive(request.getTimeSlotId(), true);
		if (!timeSlot.isPresent()) {
			return ResponseEntity.badRequest().body("Time slot not active!");
		}
		Optional<TraineeTimeSlot> traineeTimeSlotOpt = traineeTimeSlotRepo
				.findByTimeSlotIdAndTraineeEmail(request.getTimeSlotId(), request.getEmail());
		if (traineeTimeSlotOpt.isPresent())
			return ResponseEntity.badRequest().build();

		TraineeTimeSlot traineeTimeSlot = new TraineeTimeSlot();
		traineeTimeSlot.setAllowed(false);
		traineeTimeSlot.setTimeSlot(timeSlot.get());
		traineeTimeSlot.setTrainee(trainee);
		trainee.getTimeSlots().add(traineeTimeSlot);
		traineeRepo.save(trainee);
		timeSlot.get().getTrainees().add(traineeTimeSlot);
		timeSlotRepo.save(timeSlot.get());

		return ResponseEntity.ok(mapper.map(timeSlot.get(), TimeSlotDTO.class));
	}

	@PostMapping(value = "deletetimeslottrainer", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> deleteTimeslotTrainer(@RequestBody DeleteTimeslotTrainer request) {
		Optional<Trainer> user = trainerRepo.findByEmail(request.getTrainerEmail());
		if (!user.isPresent()) {
			return ResponseEntity.notFound().build();
		} else {
			Optional<TimeSlot> toDelete = timeSlotRepo.findById(request.getTimeSlotId());
			if (toDelete.isPresent()) {
				toDelete.get().setActive(false);
				timeSlotRepo.save(toDelete.get());
				return ResponseEntity.ok(mapper.map(toDelete.get(), TimeSlotDTO.class));
			} else {
				return ResponseEntity.notFound().build();
			}
		}
	}

	@PostMapping(value = "deletetimeslottrainee", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = "application/json")
	public ResponseEntity<?> deleteTimeslotTrainee(@RequestBody DeleteTimeslotTrainee request) {
		Optional<Trainee> user = traineeRepo.findByEmail(request.getEmail());
		if (!user.isPresent()) {
			return ResponseEntity.notFound().build();
		} else {
			Trainee t = user.get();
			Optional<TimeSlot> toDelete = timeSlotRepo.findById(request.getTimeSlotId());
			if (toDelete.isPresent()) {
				Optional<TraineeTimeSlot> traineeTimeSlot = traineeTimeSlotRepo
						.findByTimeSlotIdAndTraineeEmail(toDelete.get().getId(), request.getEmail());
				if (traineeTimeSlot.isPresent()) {
					t.getTimeSlots().remove(traineeTimeSlot.get());
					traineeRepo.save(t);
					traineeTimeSlotRepo.delete(traineeTimeSlot.get());
					return ResponseEntity.ok(mapper.map(toDelete.get(), TimeSlotDTO.class));
				} else {
					return ResponseEntity.notFound().build();
				}

			} else {
				return ResponseEntity.notFound().build();
			}
		}
	}

	@GetMapping(value = "getPendingTimeSlots", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getPendingTimeSlots(@RequestParam("trainerEmail") String trainerEmail) {
		List<TimeSlot> timeSlots = timeSlotRepo.findAllByTrainerEmailAndActive(trainerEmail,
				true);
		List<TraineeTimeSlot> toAllow = timeSlots.stream().flatMap(t -> t.getTrainees().stream())
				.collect(Collectors.toList());
		return ResponseEntity.ok(toAllow.stream().map(t -> mapper.map(t, TimeSlotTraineeDTO.class)));
	}

	/*
	 * @GetMapping(value = "getPendingTimeSlots", produces =
	 * MediaType.APPLICATION_JSON_UTF8_VALUE) public ResponseEntity<?>
	 * getPendingTimeSlots(@RequestParam("trainerEmail") String trainerEmail){
	 * List<TimeSlot> timeSlots =
	 * timeSlotRepo.findAllByTraineesAllowedAndTrainerEmailAndActive(false,
	 * trainerEmail, true); List<TraineeTimeSlot> toAllow = timeSlots.stream()
	 * .flatMap(t -> t.getTrainees().stream()) .collect(Collectors.toList()); return
	 * ResponseEntity.ok(toAllow.stream().map(t -> mapper.map(t,
	 * TimeSlotTraineeDTO.class))); }
	 */

	@PutMapping(value = "allowTimeSlot", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> allowTimeSlot(@RequestBody AllowTimeSlot request) {
		Optional<TimeSlot> timeSlot = timeSlotRepo.findById(request.getTimeSlotId());
		if (!timeSlot.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Optional<TraineeTimeSlot> traineeTimeSlot = timeSlot.get().getTrainees().stream()
				.filter(t -> t.getTrainee().getEmail().equals(request.getTraineeEmail())).findFirst();

		if (!traineeTimeSlot.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		traineeTimeSlot.get().setAllowed(true);

		timeSlotRepo.save(timeSlot.get());
		return ResponseEntity.ok(mapper.map(timeSlot.get(), TimeSlotDTO.class));
	}
}
