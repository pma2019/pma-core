package org.pma.PMAbackend.controller;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.modelmapper.ModelMapper;
import org.pma.PMAbackend.dto.GymDTO;
import org.pma.PMAbackend.dto.TrainerDTO;
import org.pma.PMAbackend.dto.UserDTO;
import org.pma.PMAbackend.model.Document;
import org.pma.PMAbackend.model.Gym;
import org.pma.PMAbackend.model.Trainee;
import org.pma.PMAbackend.model.Trainer;
import org.pma.PMAbackend.model.User;
import org.pma.PMAbackend.repo.GymRepo;
import org.pma.PMAbackend.repo.TraineeRepo;
import org.pma.PMAbackend.repo.TrainerRepo;
import org.pma.PMAbackend.repo.UserRepo;
import org.pma.PMAbackend.requests.UserChangePassword;
import org.pma.PMAbackend.requests.UserChangeStatus;
import org.pma.PMAbackend.requests.UserToRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/users")
public class UserController {

	@Autowired
	UserRepo userRepo;

	@Autowired
	GymRepo gymRepo;

	@Autowired
	TrainerRepo trainerRepo;
	
	@Autowired
	TraineeRepo traineeRepo;

	@Autowired
	ModelMapper mapper;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	Cloudinary cloudinary;

	@GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Page<UserDTO>> getAll(Pageable pageable) {
		Page<UserDTO> page = userRepo.findAll(pageable).map(u -> mapper.map(u, UserDTO.class));
		return ResponseEntity.ok(page);
	}

	@GetMapping(value = "getTrainers", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<TrainerDTO>> getAllTrainers(Pageable pageable) {
		List<TrainerDTO> list = trainerRepo.findAll(pageable).stream().map(u -> mapper.map(u, TrainerDTO.class)).collect(Collectors.toList());
		return ResponseEntity.ok(list);
	}

	@GetMapping(value = "getTrainer", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<TrainerDTO> getTrainer(@RequestParam("email") String email) {
		Optional<Trainer> trainer = trainerRepo.findByEmail(email);
		if (!trainer.isPresent()) {
			ResponseEntity.notFound().build();
		}
		trainer.get().setPictures(trainer.get().getPictures().stream()
				.sorted(Comparator.comparing(Document::getCreationDate).reversed()).collect(Collectors.toList()));
		return ResponseEntity.ok(mapper.map(trainer.get(), TrainerDTO.class));
	}

	@PutMapping(value = "updategeneral", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<UserDTO> updateGeneral(UserDTO userNew) {
		Optional<User> user = userRepo.findByEmail(userNew.getEmail());
		if (!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		user.get().setEmail(userNew.getEmail());
		user.get().setLastName(userNew.getLastName());
		user.get().setName(userNew.getName());
		userRepo.save(user.get());
		return ResponseEntity.ok(mapper.map(user.get(), UserDTO.class));
	}

	@PutMapping(value = "changepassword", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<UserDTO> changePassword(@RequestBody UserChangePassword request) {
		Optional<User> user = userRepo.findByEmail(request.getEmail());
		if (!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		if (!passwordEncoder.matches(request.getOldPassword(), user.get().getPassword())) {
			return ResponseEntity.notFound().build();
		}

		user.get().setPassword(passwordEncoder.encode(request.getNewPassword()));
		userRepo.save(user.get());
		return ResponseEntity.ok(mapper.map(user.get(), UserDTO.class));
	}

	@PutMapping(value = "changeprofilepicture", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<UserDTO> changeProfilePicture(@RequestParam("image") MultipartFile image,
			@RequestParam("email") String email) {
		Optional<User> user = userRepo.findByEmail(email);
		if (!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		try {
			File file = Files.createTempFile("temp_" + user.get().getEmail(), image.getOriginalFilename()).toFile();
			image.transferTo(file);
			@SuppressWarnings("unchecked")
			Map<String, Object> uploadResult = cloudinary.uploader().upload(file,
					ObjectUtils.asMap("use_filename", true, "unique_filename", false, "resource_type", "auto"));
			String filePath = (String) uploadResult.get("url");
			user.get().getProfilePicture().setStorageUrl(filePath);
			user.get().getProfilePicture().setUpdateDate(new Date());
			userRepo.save(user.get());
			return ResponseEntity.ok(mapper.map(user.get(), UserDTO.class));
		} catch (IOException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}
	}

	@GetMapping(value = "getNearBy", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getNearBy(@RequestParam("latitude") Double latitude, @RequestParam("longitude") Double longitude) {
		int earthRadiusKm = 6371;
		List<Gym> gymList = gymRepo.findAll();
		List<Gym> nearBy = new ArrayList<>();
		for (Gym g : gymList) {
			double rLat = Math.toRadians(latitude-g.getLatitude());
			double rLon = Math.toRadians(longitude-g.getLongitude());
			double rLat1 = Math.toRadians(latitude);
			double rLat2 = Math.toRadians(latitude);

			double a = Math.pow(Math.sin(rLat/2), 2.0) + Math.pow(Math.sin(rLon/2), 2.0) * Math.cos(rLat1) * Math.cos(rLat2);
			double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			double range = earthRadiusKm * c;
			if(range <= 2.0)
				nearBy.add(g);
		}
		return ResponseEntity.ok(nearBy.stream().map(g -> mapper.map(g, GymDTO.class)));
	}

	@PutMapping(value = "changeactive", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<UserDTO> changeStatus(@RequestBody UserChangeStatus request) {
		Optional<User> user = userRepo.findByEmail(request.getEmail());
		if (user.isPresent()) {
			user.get().setIsActive(request.getStatus());
			userRepo.save(user.get());
			return ResponseEntity.ok(mapper.map(user.get(), UserDTO.class));
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PostMapping(value = "register", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = "application/json")
	public ResponseEntity<Boolean> register(@RequestBody UserToRegister request) {
		Optional<User> user = userRepo.findByEmail(request.getEmail());
		if (user.isPresent()) {
			return ResponseEntity.noContent().build();
		} else {
			user = userRepo.findByUsername(request.getUsername());
			if (user.isPresent()) {
				return ResponseEntity.noContent().build();
			} else {
				Document d = new Document();
				d.setCreationDate(new Date());
				d.setIsActive(true);
				d.setName("default_pic");
				d.setStorageUrl("https://res.cloudinary.com/dmy5pjkae/image/upload/v1555358125/83959218-muscular-man-flexing-biceps-avatar-fitness-icon-image-vector-illustration-design_cpeokg.jpg");
				if(request.getDtype().equals("trainee")) {
				    Trainee trainee = new Trainee();
				    trainee.setUsername(request.getUsername());
				    trainee.setEmail(request.getEmail());
				    trainee.setIsActive(true);
				    trainee.setLastName(request.getLastName());
				    trainee.setName(request.getName());
					trainee.setPassword(passwordEncoder.encode(request.getPassword()));
					trainee.setRegistrationDate(new Date());
					trainee.setProfilePicture(d);
					trainee = traineeRepo.save(trainee);

					if (trainee != null)
						return ResponseEntity.ok(true);
				}else {
					Gym g = gymRepo.findById(request.getGymId()).get();
				    Trainer trainer = new Trainer();
				    trainer.setGym(g);
				    trainer.setBio(request.getBio());
				    trainer.setUsername(request.getUsername());
				    trainer.setEmail(request.getEmail());
					trainer.setIsActive(true);
					trainer.setLastName(request.getLastName());
					trainer.setName(request.getName());
					trainer.setPassword(passwordEncoder.encode(request.getPassword()));
					trainer.setRegistrationDate(new Date());
					trainer.setProfilePicture(d);
					trainer = trainerRepo.save(trainer);

					if (trainer != null)
						return ResponseEntity.ok(true);
				}
				return ResponseEntity.notFound().build();
			}
		}
	}
}
