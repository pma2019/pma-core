package org.pma.PMAbackend.controller;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.modelmapper.ModelMapper;
import org.pma.PMAbackend.dto.DocumentDTO;
import org.pma.PMAbackend.model.Document;
import org.pma.PMAbackend.model.Trainer;
import org.pma.PMAbackend.model.User;
import org.pma.PMAbackend.repo.DocumentRepo;
import org.pma.PMAbackend.repo.TrainerRepo;
import org.pma.PMAbackend.repo.UserRepo;
import org.pma.PMAbackend.requests.DocumentChangeStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(value = "/documents")
public class DocumentController {
    @Autowired
    DocumentRepo documentRepo;

    @Autowired
    Cloudinary cloudinary;

    @Autowired
    TrainerRepo trainerRepo;

    @Autowired
    UserRepo userRepo;

    @Autowired
    ModelMapper mapper;




    @GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAll(Pageable pageable) {
        return ResponseEntity.ok(documentRepo.findAll(pageable).map(d -> mapper.map(d, DocumentDTO.class)));
    }

    @GetMapping(value = "byUser", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllByUser(Pageable pageable, @RequestParam(value = "email") String email) {
        return ResponseEntity.ok(documentRepo.findByUserEmail(email, pageable).map(d -> mapper.map(d, DocumentDTO.class)));
    }

    @PutMapping(value = "changeactive",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> changeStatus(@RequestBody DocumentChangeStatus request){
        Optional<Document> documentOptional = documentRepo.findById(request.getId());
        if(documentOptional.isPresent()){
            documentOptional.get().setIsActive(request.getStatus());
            documentRepo.save(documentOptional.get());
            return ResponseEntity.ok(mapper.map(documentOptional.get(), DocumentDTO.class));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(value = "addpicture", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> addPicture(@RequestParam("image") MultipartFile image,
                                        @RequestParam("email") String email,
                                        @RequestParam("avatar") Boolean avatar) {
        //User user = null;
        Optional<? extends User> userOpt = null;
        if(avatar) {
            userOpt = userRepo.findByEmail(email);
        } else {
            userOpt = trainerRepo.findByEmail(email);
        }
        if(!userOpt.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        try {
            File file = Files.createTempFile(userOpt.get().getEmail(), image.getOriginalFilename()).toFile();
            image.transferTo(file);
            @SuppressWarnings("unchecked")
			Map<String, Object> uploadResult = cloudinary.uploader().upload(file, ObjectUtils
                    .asMap("use_filename", true,
                            "unique_filename", false,
                            "resource_type", "auto"));
            String filePath = (String) uploadResult.get("url");
            Document newDocument = new Document();
            newDocument.setCreationDate(new Date());
            newDocument.setIsActive(true);
            newDocument.setStorageUrl(filePath);
            newDocument.setName(file.getName());
            if(avatar) {
                userOpt.get().setProfilePicture(newDocument);
                userRepo.save(userOpt.get());
                newDocument = userOpt.get().getProfilePicture();
            } else {
                newDocument.setTrainer((Trainer)userOpt.get());
                documentRepo.save(newDocument);
            }
            return ResponseEntity.ok(mapper.map(newDocument, DocumentDTO.class));
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }


}
