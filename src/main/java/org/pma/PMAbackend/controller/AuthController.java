package org.pma.PMAbackend.controller;

import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.pma.PMAbackend.dto.UserDTO;
import org.pma.PMAbackend.model.User;
import org.pma.PMAbackend.repo.UserRepo;
import org.pma.PMAbackend.security.JwtAuthenticationRequest;
import org.pma.PMAbackend.security.JwtAuthenticationResponse;
import org.pma.PMAbackend.security.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/auth")
public class AuthController {


    @Autowired
    private UserRepo userRepo;


    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService myAppUserDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;



    @Value("Authorization")
    private String tokenHeader;

    @Autowired
    ModelMapper mapper;

    @Value("mySecret")
    private String secret;


    @GetMapping(value="/getone")
    public ResponseEntity<?> getByUsername(@RequestParam("emalil") String email) {
        Optional<User> user = userRepo.findByEmail(email);
        if(user.isPresent()) {
            if(user.get().getIsActive()) {
                return ResponseEntity.status(403).build();
            }
            return ResponseEntity.ok(mapper.map(user.get(), UserDTO.class));
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User not found");
        }
    }

    @RequestMapping(value="/login",
            method=RequestMethod.POST,
            consumes= {"application/json", "application/json;charset=UTF-8"},
            produces= {"application/json"})
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest) throws AuthenticationException {
        try{
            Optional<User> user = userRepo.findByEmail(authenticationRequest.getUsername());


            if(user.isPresent()) {
                if(user.get().getIsActive()) {
                    final Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                            authenticationRequest.getUsername(),
                            authenticationRequest.getPassword()));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    final UserDetails userDetails = myAppUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());

                    final String token = jwtTokenUtil.generateToken(userDetails);
                    return ResponseEntity.ok(new JwtAuthenticationResponse(token, mapper.map(user.get(), UserDTO.class),
                            userDetails.getAuthorities()
                                    .stream()
                                    .map(r -> r.getAuthority())
                                    .collect(Collectors.toList())));
                } else {
                    return ResponseEntity.status(403).build();
                }
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User not found");
            }
        } catch (BadCredentialsException | UsernameNotFoundException e){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
        }
    }

   /* @PostMapping(value="/register",
            produces=MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> insert(@RequestBody UserCreation newEntity) {
        Optional<User> found = userRepo.findOneByUsername(newEntity.getUsername());
        if(found.isPresent()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        RegisteredUser newUser = mapper.map(newEntity, RegisteredUser.class);
        newUser.setBlocked(false);
        newUser.setReservations(new HashSet<>());
        newUser.setUserImpression(new HashSet<>());
        newUser.setPassword(passwordEncoder.encode(newEntity.getPassword()));
        newUser.setRegistrationDate(new Date());
        return new ResponseEntity<RegisteredUserView>(mapper.map(registeredUserRepo.save(newUser), RegisteredUserView.class), HttpStatus.OK);
    }
*/
   @PostMapping(value = "cryptme", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
   public String cryptMe(@RequestParam("password") String password) {
       return passwordEncoder.encode(password);
   }

   @GetMapping(value = "hello")
   public String hello(){
       return "hello";
   }
}
