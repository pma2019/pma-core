package org.pma.PMAbackend.controller;

import org.modelmapper.ModelMapper;
import org.pma.PMAbackend.dto.MessageDTO;
import org.pma.PMAbackend.model.Message;
import org.pma.PMAbackend.model.User;
import org.pma.PMAbackend.repo.MessageRepo;
import org.pma.PMAbackend.repo.UserRepo;
import org.pma.PMAbackend.requests.AddMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping(value = "/chat")
public class ChatController {
    @Autowired
    UserRepo userRepo;

    @Autowired
    MessageRepo messageRepo;

    @Autowired
    ModelMapper modelMApper;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;


    @GetMapping(value = "/getMessagesFrom", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getMessages(@RequestParam("email") String email) {
        String loggedInEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        return ResponseEntity.ok(messageRepo.findAllMessageWith(email, loggedInEmail)
                .stream()
                .map(m -> modelMApper.map(m, MessageDTO.class)));
    }

    @GetMapping(value = "/getInbox", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getInbox() {
        String loggedInEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Message> inbox = new ArrayList<>();
        List<Message> messages = messageRepo.findAllByReceiverEmailOrSenderEmailOrderBySentDateDesc(loggedInEmail, loggedInEmail);

        HashMap<String, Message> messageMap = new HashMap<>();

        for(Message m : messages) {
            if(!messageMap.containsKey(m.getSender().getEmail()) && !messageMap.containsKey(m.getReceiver().getEmail())) {
                inbox.add(m);
                messageMap.put(m.getSender().getEmail(), m);
            }
        }
        return ResponseEntity.ok(inbox.stream().map(m -> modelMApper.map(m, MessageDTO.class)));
    }

    @RequestMapping(value = "/sendMessage",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MessageDTO> createMessage(@RequestBody AddMessage request) {
        Message message = new Message();
        message.setSentDate(new Date());
        message.setBody(request.getText());
        message.setIsOpen(false);
        Optional<User> sender = userRepo.findByEmail(request.getSenderEmail());
        if(!sender.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Optional<User> receiver = userRepo.findByEmail(request.getReceiverEmail());
        if(!receiver.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        message.setSender(sender.get());
        message.setReceiver(receiver.get());
        messageRepo.save(message);
        MessageDTO returnMsg = modelMApper.map(message, MessageDTO.class);
        simpMessagingTemplate.convertAndSend("/topic/reply", returnMsg);
        return ResponseEntity.ok(returnMsg);
    }

    /*
    @MessageMapping("/chat.sendMessage")
    public MessageDTO sendMessage(@Payload MessageDTO chatMessage) throws Exception {
        return chatMessage;
    }

    @MessageMapping("/chat.addUser")
    public MessageDTO addUser(@Payload MessageDTO chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        headerAccessor.getSessionAttributes().put("email", chatMessage.getSenderEmail());
        return chatMessage;
    }

    @MessageExceptionHandler
    @SendToUser("/queue/errors")
    public String handleExceptions(Throwable exc) {
        return exc.getMessage();
    }
    */

}
