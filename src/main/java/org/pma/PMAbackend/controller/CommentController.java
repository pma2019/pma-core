package org.pma.PMAbackend.controller;

import org.modelmapper.ModelMapper;
import org.pma.PMAbackend.dto.CommentDTO;
import org.pma.PMAbackend.model.Comment;
import org.pma.PMAbackend.model.TimeSlot;
import org.pma.PMAbackend.model.TraineeTimeSlot;
import org.pma.PMAbackend.model.User;
import org.pma.PMAbackend.repo.CommentRepo;
import org.pma.PMAbackend.repo.TimeSlotRepo;
import org.pma.PMAbackend.repo.TraineeTimeSlotRepo;
import org.pma.PMAbackend.repo.UserRepo;
import org.pma.PMAbackend.requests.AddComment;
import org.pma.PMAbackend.requests.CommentChangeStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Optional;

@RestController
@RequestMapping(value = "/comments")
public class CommentController {


	//TODO TRAINER REPLY, TRAINEE CAN COMMENT MORE THAN ONCE

	@Autowired
	CommentRepo commentRepo;

	@Autowired
	ModelMapper modelMapper;

	@Autowired
	UserRepo userRepo;

	@Autowired
	TimeSlotRepo timeSlotRepo;

	@Autowired
	TraineeTimeSlotRepo traineeTimeSlotRepo;

	@GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getAll(@NotNull final Pageable pageable) {
		return ResponseEntity.ok(commentRepo.findAll(pageable).map(c -> modelMapper.map(c , CommentDTO.class)));
	}

	@GetMapping(value = "byUser", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getAllByUser(Pageable pageable, @RequestParam(value = "email") String email) {
		return ResponseEntity.ok(commentRepo.findByCreateByEmailAndIsActive(email, true)
				.stream()
				.map(c -> modelMapper.map(c , CommentDTO.class)));
	}

	@GetMapping(value = "byTimeSlot", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getAllByTimeSlot(Pageable pageable, @RequestParam(value = "timeSlotId") Integer timeSlotId) {
		return ResponseEntity.ok(commentRepo.findByTimeSlotIdAndIsActiveAndByTrainer(timeSlotId, true, false)
				.stream()
				.map(c -> modelMapper.map(c , CommentDTO.class)));
	}

	@PostMapping(value = "addComment", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> addComment(@RequestBody AddComment request) {
		Optional<Comment> commentOptional = commentRepo.findByCreateByEmailAndTimeSlotIdAndIsActiveAndByTrainer(request.getCreateByEmail(),
				request.getTimeSlotId(),
				true,
				false);
		if(commentOptional.isPresent()) {
			return ResponseEntity.badRequest().body("Cant't add more than one comment per appointment");
		}
		Comment newComment = new Comment();
		newComment.setBody(request.getBody());
		newComment.setCreationDate(new Date());
		newComment.setIsActive(true);
		newComment.setUpdateDate(null);
		newComment.setByTrainer(request.getByTrainer());
		Optional<User> user = userRepo.findByEmail(request.getCreateByEmail());
		if(user.isPresent()){
			newComment.setCreateBy(user.get());
		} else {
			return ResponseEntity.badRequest().body("User doesn't exist");
		}
		Optional<TimeSlot> timeSlot = timeSlotRepo.findById(request.getTimeSlotId());
		if(!timeSlot.isPresent()) {
			return ResponseEntity.badRequest().body("Time Slot doesn't exist");
		}
		if(!request.getByTrainer()) {
			Optional<TraineeTimeSlot> traineeTimeSlot = traineeTimeSlotRepo
					.findByTimeSlotIdAndTraineeEmail(request.getTimeSlotId(), request.getCreateByEmail());
			if(!traineeTimeSlot.isPresent() || !traineeTimeSlot.get().isAllowed()) {
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
			}
		}
		newComment.setTimeSlot(timeSlot.get());
		if(newComment.getByTrainer()) {
			Optional<Comment> parentComment = commentRepo.findByIdAndIsActive(request.getParentId(), true);
			if(!parentComment.isPresent())
				return ResponseEntity.badRequest().body("Parent comment must exist.");
			if(parentComment.get().getReply() != null)
				return ResponseEntity.badRequest().body("You have already replied to the trainee.");
			parentComment.get().setReply(newComment);
			newComment.setReplyOn(parentComment.get());
			commentRepo.save(parentComment.get());
			return ResponseEntity.ok(modelMapper.map(parentComment.get().getReply(), CommentDTO.class));
		} else {
			newComment = commentRepo.save(newComment);
			return ResponseEntity.ok(modelMapper.map(newComment, CommentDTO.class));
		}


	}

	@PutMapping(value = "changeactive", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> changeStatus(@RequestBody CommentChangeStatus request) {
		Optional<Comment> commentOpt = commentRepo.findById(request.getId());
		if (commentOpt.isPresent()) {
			commentOpt.get().setIsActive(request.getStatus());
			commentRepo.save(commentOpt.get());
			return ResponseEntity.ok(modelMapper.map(commentOpt.get(), CommentDTO.class));
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}
