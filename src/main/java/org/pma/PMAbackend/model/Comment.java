package org.pma.PMAbackend.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Getter
@Setter
public class Comment {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;
    private Date creationDate;
    private Date updateDate;
    private Boolean isActive;
    private Boolean byTrainer;
    private String body;

    @ManyToOne
    private TimeSlot timeSlot;

    @ManyToOne
    private User createBy;

    @OneToOne(mappedBy = "reply")
    private Comment replyOn;

    @OneToOne
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Comment reply;

}
