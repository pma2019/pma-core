package org.pma.PMAbackend.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(
        name="timeslot_trainee",
        uniqueConstraints=
        @UniqueConstraint(columnNames={"trainee_id", "time_slot_id"})
)
@NoArgsConstructor
@Getter
@Setter
public class TraineeTimeSlot {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @ManyToOne
    private Trainee trainee;

    @ManyToOne
    private TimeSlot timeSlot;

    private boolean allowed;

}
