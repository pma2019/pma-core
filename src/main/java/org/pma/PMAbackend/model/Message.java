package org.pma.PMAbackend.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Getter @Setter
public class Message {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;
    private Date sentDate;
    private Boolean isOpen;
    private String body;

    @ManyToOne
    private User sender;
    @ManyToOne
    private User receiver;
}
