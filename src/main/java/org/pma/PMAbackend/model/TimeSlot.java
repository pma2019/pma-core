package org.pma.PMAbackend.model;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.util.List;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Getter
@Setter
@JsonIgnoreProperties
public class TimeSlot {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;
	@Range(min = 2019, max = 2030)
	private Integer year;
	@Range(min = 1, max = 12)
	private Integer month;
	@Range(min = 1, max = 31)
	private Integer day;
	@Range(min = 0, max = 23)
	private Integer hour;
	

	private boolean active;

	@ManyToOne
	@Cascade({ CascadeType.ALL })
	private Trainer trainer;

	@OneToMany(mappedBy = "timeSlot")
	private List<TraineeTimeSlot> trainees;

	@OneToMany(mappedBy = "timeSlot")
	private List<Comment> comments;

	@OneToMany(mappedBy = "timeSlot")
	private List<Rating> ratings;

	@Transient
	private Float overAllRatings;

	public Float getOverAllRatings() {
		int count = getRatings().size();
		if(count > 0) {
			Float sum = getRatings().stream().map(Rating::getRatingNumber).reduce(0F, Float::sum);
			return sum / count;
		} else {
			return (float)0;
		}


	}
}
