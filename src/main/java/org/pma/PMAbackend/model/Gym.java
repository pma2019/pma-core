package org.pma.PMAbackend.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@NoArgsConstructor
@Getter @Setter
public class Gym {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;

    private String name;

    private Double longitude;

    private Double latitude;

    @OneToMany(mappedBy = "gym")
    private List<Trainer> trainers;
}
