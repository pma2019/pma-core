package org.pma.PMAbackend.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


import javax.persistence.*;
import java.util.List;
import java.util.Optional;

@Entity
@Getter
@Setter
@DiscriminatorValue("trainer")
public class Trainer extends User {

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "trainer")
    @Cascade(CascadeType.ALL)
    private List<TimeSlot> timeSlots;
    private String bio;

    @OneToMany(mappedBy = "trainer")
    @Cascade(CascadeType.ALL)
    private List<Document> pictures;

    @ManyToOne
    private Gym gym;
    
    @Transient
    private Float overallRatings;



    public Float getOverallRatings() {
        if (overallRatings == null) {
            Optional<Integer> count = timeSlots.stream().map(t -> t.getRatings().size()).reduce(Integer::sum);
            if (count.isPresent() && count.get() > 0) {
                Float sum = timeSlots.stream()
                        .flatMap(t -> t.getRatings().stream())
                        .map(Rating::getRatingNumber)
                        .reduce(0F, Float::sum);
                overallRatings = sum / count.get();
            }
        }
        overallRatings = overallRatings != null ? overallRatings : (float) 0;
        return overallRatings;
    }

    @Override
    public String getDTYPE() {
        return "trainer";
    }
}
