package org.pma.PMAbackend.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

import java.util.List;

@Entity
@Getter
@Setter
@DiscriminatorValue("trainee")
public class Trainee extends User {

    @OneToMany(mappedBy = "trainee")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<TraineeTimeSlot> timeSlots;

    @Override
    public String getDTYPE() {
        return "trainee";
    }
}
