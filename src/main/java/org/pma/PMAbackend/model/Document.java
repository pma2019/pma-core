package org.pma.PMAbackend.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Getter
@Setter
public class Document {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;
    private String name;
    private String storageUrl;
    private Date creationDate;
    private Date updateDate;
    private Boolean isActive;

    @ManyToOne
    private Trainer trainer;

    @OneToOne(mappedBy = "profilePicture", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private User user;
}
