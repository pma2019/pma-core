package org.pma.PMAbackend.security;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.pma.PMAbackend.dto.UserDTO;

import java.io.Serializable;
import java.util.List;
@Getter @Setter @NoArgsConstructor
public class JwtAuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 1250166508152483573L;

    private String token;

    private UserDTO userProfile;
    private List<String> roles;


    public JwtAuthenticationResponse(String token, UserDTO user, List<String> roles) {
        super();
        this.token = token;
        this.userProfile = user;
        this.roles = roles;
    }

}