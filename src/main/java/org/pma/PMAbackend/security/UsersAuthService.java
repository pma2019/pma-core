package org.pma.PMAbackend.security;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.pma.PMAbackend.model.User;
import org.pma.PMAbackend.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;



@Service("myAppUserDetailsService")
public class UsersAuthService implements UserDetailsService {

    @Autowired
    private UserRepo userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userDao.findByEmail(username);
        if(!user.isPresent())
            throw new UsernameNotFoundException("Username not found.");

        List<SimpleGrantedAuthority> roleAuths = new ArrayList<>();
        roleAuths.add(new SimpleGrantedAuthority("ROLE_" + user.get().getDTYPE()));
        return new UserDetailsCustom(user.get().getEmail(), user.get().getPassword(), roleAuths);
    }

}