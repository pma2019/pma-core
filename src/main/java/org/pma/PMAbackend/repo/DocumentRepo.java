package org.pma.PMAbackend.repo;

import org.pma.PMAbackend.model.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepo extends PagingAndSortingRepository<Document, Integer> {
    @Query("SELECT d FROM Document d JOIN d.trainer JOIN d.user " +
            "WHERE d.trainer.email like :email or d.user.email like :email")
    Page<Document> findByUserEmail(@Param("email") String email, Pageable pageable);
}
