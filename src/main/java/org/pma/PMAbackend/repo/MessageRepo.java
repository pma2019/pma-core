package org.pma.PMAbackend.repo;

import org.pma.PMAbackend.model.Message;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepo extends PagingAndSortingRepository<Message, Integer> {
    @Query("SELECT m FROM Message m JOIN m.sender JOIN m.receiver " +
            "WHERE (m.sender.email like :myEmail and m.receiver.email like :chateeEmail)" +
            "OR (m.sender.email like :chateeEmail and m.receiver.email like :myEmail) ORDER BY m.sentDate")
    List<Message> findAllMessageWith(String myEmail, String chateeEmail);

    List<Message> findAllByReceiverEmailOrSenderEmailOrderBySentDateDesc(String senderEmail, String receiverEmail);
}