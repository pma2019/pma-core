package org.pma.PMAbackend.repo;

import org.pma.PMAbackend.model.Comment;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommentRepo extends PagingAndSortingRepository<Comment, Integer> {
    Optional<Comment> findByCreateByEmailAndTimeSlotIdAndIsActiveAndByTrainer(String email, Integer id, boolean isActive, boolean byTrainer);
    List<Comment> findByCreateByEmailAndIsActive(String email, boolean isActive);
    List<Comment> findByTimeSlotIdAndIsActiveAndByTrainer(Integer id, boolean isActive, boolean byTrainer);
    Optional<Comment> findByIdAndIsActive(Integer id, boolean isActive);
}
