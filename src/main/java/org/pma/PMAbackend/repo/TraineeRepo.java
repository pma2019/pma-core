package org.pma.PMAbackend.repo;

import org.pma.PMAbackend.model.Trainee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TraineeRepo extends PagingAndSortingRepository<Trainee, Integer> {
    Optional<Trainee> findByEmail(String email);
    Optional<Trainee> findById(Integer id);
	Page<Trainee> findAllById(Integer timeSlotId, Pageable pageable);
}
