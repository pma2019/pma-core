package org.pma.PMAbackend.repo;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.pma.PMAbackend.model.User;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends PagingAndSortingRepository<User, Integer> {
    Optional<User> findByEmail(String email);

	Optional<User> findByUsername(String username);
}
