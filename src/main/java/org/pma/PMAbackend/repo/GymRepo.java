package org.pma.PMAbackend.repo;

import org.pma.PMAbackend.model.Gym;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GymRepo extends PagingAndSortingRepository<Gym, Integer> {
    List<Gym> findAll();
}
