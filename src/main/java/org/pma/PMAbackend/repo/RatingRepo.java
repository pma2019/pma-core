package org.pma.PMAbackend.repo;

import org.pma.PMAbackend.model.Rating;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RatingRepo extends PagingAndSortingRepository<Rating, Integer> {
    List<Rating> findByTimeSlotId(Integer id);
    List<Rating> findByTimeSlotTrainerEmail(String email);
    Optional<Rating> findByTimeSlotIdAndTraineeEmail(Integer timeSlotId, String traineeEmail);
}

