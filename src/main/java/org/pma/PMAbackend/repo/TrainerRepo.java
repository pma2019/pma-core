package org.pma.PMAbackend.repo;

import org.pma.PMAbackend.model.Trainer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TrainerRepo extends PagingAndSortingRepository<Trainer, Integer> {
    List<Trainer> findAll();
    Optional<Trainer> findByEmail(String email);
    Optional<Trainer> findById(Integer id);
}
