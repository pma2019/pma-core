package org.pma.PMAbackend.repo;

import org.pma.PMAbackend.model.TimeSlot;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface TimeSlotRepo extends PagingAndSortingRepository<TimeSlot, Integer> {
    List<TimeSlot> findAllByTrainerEmailAndActive(String email, boolean active);
    List<TimeSlot> findAllByTraineesTraineeEmailAndActive(String email, boolean active);
    List<TimeSlot> findAllByTraineesAllowedAndTrainerEmailAndActive(boolean allowed, String trainerEmail, boolean active);
    Optional<TimeSlot> findByIdAndTraineesTraineeEmailAndActive(Integer id, String email, boolean active);
    Optional<TimeSlot> findByIdAndTraineesTraineeEmailAndTraineesAllowed(Integer id, String email, boolean allowed);
    Optional<TimeSlot> findByIdAndActive(Integer id, boolean active);

    List<TimeSlot> findAll();
    Optional<TimeSlot> findByTrainerEmailAndYearAndMonthAndDayAndHour(String email,Integer year,Integer month, Integer day, Integer hours);
}
