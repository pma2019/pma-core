package org.pma.PMAbackend.repo;

import java.util.List;
import java.util.Optional;

import org.pma.PMAbackend.model.TraineeTimeSlot;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TraineeTimeSlotRepo extends PagingAndSortingRepository<TraineeTimeSlot, Integer>{
	Optional<TraineeTimeSlot> findById(Integer id);
	Optional<TraineeTimeSlot> findByTimeSlotId(Integer id);
	Optional<TraineeTimeSlot> findByTimeSlotIdAndTraineeEmail(Integer id, String traineeEmail);
	List<TraineeTimeSlot> findAllByTraineeEmail(String email);

}
